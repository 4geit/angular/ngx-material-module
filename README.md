# @4geit/ngx-material-module [![npm version](//badge.fury.io/js/@4geit%2Fngx-material-module.svg)](//badge.fury.io/js/@4geit%2Fngx-material-module)

---

This is an all-in-one angular module to export all the `Angular Material` related components so it saves time to export all the components at once. Here are the list of the packages exported:

* @angular/material,
* @angular/flex-layout,
* hammerjs
* @swimlane/ngx-datatable
* material-design-icons
* font-awesome
* roboto-fontface

## Installation

1. A recommended way to install ***@4geit/ngx-material-module*** is through [npm](https://www.npmjs.com/search?q=@4geit/ngx-material-module) package manager using the following command:

```bash
npm i @4geit/ngx-material-module --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/ngx-material-module
```

2. You need to import the `NgxMaterialModule` class in whatever module you want in your project for instance `app.module.ts` as follows:

```js
import { NgxMaterialModule } from '@4geit/ngx-material-module';
```

And you also need to add the `NgMaterialModule` class within the `@NgModule` decorator as part of the `imports` list:

```js
@NgModule({
  // ...
  imports: [
    // ...
    NgxMaterialModule,
    // ...
  ],
  // ...
})
export class AppModule { }
```

3. You also need to import some CSS style files. If you are using `@angular/cli` to manage your Angular project, you can simply add those paths in the `.angular-cli.json` file under the `styles` property:

```json
"styles": [
  "../node_modules/material-design-icons/iconfont/material-icons.css",
  "../node_modules/font-awesome/css/font-awesome.css",
  "../node_modules/roboto-fontface/css/roboto/roboto-fontface.css",
]
```
