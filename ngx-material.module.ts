import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FlexLayoutModule } from '@angular/flex-layout';
import {
  MdButtonModule, MdCheckboxModule, MdCardModule,
  MdToolbarModule, MdGridListModule, MdInputModule,
  MdSnackBarModule, MdRadioModule, MdIconModule,
  MdDialogModule, MdProgressBarModule,
  MdAutocompleteModule, MdSidenavModule,
  MdListModule, MdTabsModule, MdTooltipModule,
  MdSlideToggleModule, MdSliderModule,
  MdMenuModule, MdProgressSpinnerModule,
  MdButtonToggleModule, MdChipsModule } from '@angular/material';
import 'hammerjs';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MdButtonModule,
    MdCheckboxModule,
    MdCardModule,
    MdToolbarModule,
    MdGridListModule,
    MdInputModule,
    MdSnackBarModule,
    MdRadioModule,
    MdIconModule,
    MdDialogModule,
    MdProgressBarModule,
    MdAutocompleteModule,
    MdSidenavModule,
    MdListModule,
    MdTabsModule,
    MdTooltipModule,
    MdSlideToggleModule,
    MdSliderModule,
    MdMenuModule,
    MdProgressSpinnerModule,
    MdButtonToggleModule,
    MdChipsModule,
    NgxDatatableModule,
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MdButtonModule,
    MdCheckboxModule,
    MdCardModule,
    MdToolbarModule,
    MdGridListModule,
    MdInputModule,
    MdSnackBarModule,
    MdRadioModule,
    MdIconModule,
    MdDialogModule,
    MdProgressBarModule,
    MdAutocompleteModule,
    MdSidenavModule,
    MdListModule,
    MdTabsModule,
    MdTooltipModule,
    MdSlideToggleModule,
    MdSliderModule,
    MdMenuModule,
    MdProgressSpinnerModule,
    MdButtonToggleModule,
    MdChipsModule,
    NgxDatatableModule,
  ]
})
export class NgxMaterialModule { }
